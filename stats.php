<?php
require_once('config.php');
include_once('functions.php');
require_once('libs/Smarty.class.php');

$smarty = new Smarty();

$periodeEnCours = new DateTime("-1day");
$h = intval($periodeEnCours->format('G'));
$periodeEnCours->setTime($h - ($h % 6), 0);
$filtreDate = $periodeEnCours->format('Y-m-d H:i:s');

//On récupère toutes les resume
$requete = $pdo->query('SELECT r.*, s.insee 
FROM resumeStatus r 
INNER JOIN stations s ON s.code = r.code 
WHERE r.date >= "'.$filtreDate.'" AND r.duree = 360 ORDER BY r.code, r.date ASC');
$resumes = $requete->fetchAll();

//On parcourt les résumes, et on agglomère par station
$stations = array();
$minDate = null;
$maxDate = null;
foreach($resumes as $resume)
{
    $codeStation = $resume['code'];
    if(!isset($stations[$codeStation]))
        $stations[$codeStation] = array('pris' => 0, 'rendu' => 0, 'insee' => $resume['insee']);
    $stations[$codeStation]['pris'] += $resume['nbBikePris'] + $resume['nbEBikePris'];
    $stations[$codeStation]['rendu'] += $resume['nbBikeRendu'] + $resume['nbEBikeRendu'];

    if($minDate == null || $minDate > $resume['date'])
        $minDate = $resume['date'];
    if($maxDate == null || $maxDate < $resume['date'])
        $maxDate = $resume['date'];
}

$minDateObj = new DateTime($minDate);
$maxDateObj = new DateTime($maxDate);
$maxDateObj->add(new DateInterval('PT6H')); //On ajoute 6 heures pour avoir la fin de la période
$smarty->assign(array(
    'minDate' => $minDateObj->format('d/m à H:i'),
    'maxDate' => $maxDateObj->format('d/m à H:i')
));

//On fait les stats sur les stations
$stationsSansMouvement = 0;
$totalPris = 0;
$totalRendu = 0;
$depts = array();
$nomDept = array(
    75 => 'Paris',
    78 => 'Yvelines',
    91 => 'Essonne',
    92 => 'Hauts-de-Seine',
    93 => 'Seine-Saint-Denis',
    94 => 'Val-de-Marne',
    95 => 'Val-d\'Oise'
);
foreach($stations as $code => $stats)
{
    if($stats['pris'] + $stats['rendu'] == 0)
        $stationsSansMouvement++;
    else
    {
        $totalPris += $stats['pris'];
        $totalRendu += $stats['rendu'];
    }

    $deptStation = floor($stats['insee'] / 1000);
    if(!isset($depts[$deptStation]))
        $depts[$deptStation] = array('nb' => 1, 'nom' => (isset($nomDept[$deptStation]) ? $nomDept[$deptStation] : 'Inconnu'));
    else
        $depts[$deptStation]['nb']++;
}

$smarty->assign(array(
    'totalPris' => $totalPris,
    'totalRendu' => $totalRendu,
    'totalStation' => count($stations),
    'stationsSansMouvement' => $stationsSansMouvement
));

$smarty->assign(array(
    'departements' => $depts
));

$smarty->display('stats.tpl');
exit();
?>