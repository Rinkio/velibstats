{include file='header.tpl'}
<header>
    <h1>Vélib Stats (site non officiel) - Statistiques</h1>
    <nav>
        <a href="index.php">&lt; Retour à l'accueil</a>
    </nav>
</header>
<div id="content">
    <p>Sur la dernière journée ({$minDate} → {$maxDate}) :</p>
    <ul>
        <li>Au moins {$totalPris} vélos ont été retirés;</li>
        <li>Au moins {$totalRendu} vélos ont été remis;</li>
        <li>Nombre de stations sans mouvement : {$stationsSansMouvement}, sur un total de {$totalStation} stations.</li>
    </ul>
    <p>Nombre de stations par départements :</p>
    <ul>
        {foreach $departements as $code => $departement}
            <li>{$code} {$departement.nom} : {$departement.nb}
        {/foreach}
    </ul>
    <p>Les nombres de vélos empruntés et rendus sont obtenu en comparant deux relevés d'état d'une station. 
    Ainsi, le système considèrera de la même façon si deux vélos ont été pris à une station que si dans la même minute 
    quatres vélos ont été pris et deux rendus. 
    Les nombres ainsi obtenus ne sont que des minimums, et comprennent à la fois les locations comme les actions de régulations.<br />
    Le nombre de stations ne différencie pas les stations en travaux des stations ouvertes.
    </p>
    {include file="credits.tpl"}
</div>
{include file="footer.tpl"}